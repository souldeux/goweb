package middleware

import (
	"log"
	"net/http"
)

//Middleware
func ConsoleLoggingMiddleware(next http.Handler) http.Handler {
	//LoggingMiddleware logs the method and URI of each request
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		go func() {
			log.Println(r.Method, r.RequestURI)
		}()
		//Must always call the next handler, otherwise middleware will abort request
		next.ServeHTTP(w, r)
	})
}
