package models

type Book struct {
	ID     int    `json:"id"`
	Title  string `json:"title,omitempty"`
	Author string `json:"author,omitempty"`
}

func (b *Book) Meta() *metadata {
	ConcreteFieldInstructions := map[string]string{
		"ID":     "serial PRIMARY KEY",
		"Title":  "VARCHAR (255) NOT NULL",
		"Author": "VARCHAR (255) NOT NULL",
	}

	return &metadata{
		Table:                     "books",
		SingularName:              "book",
		PluralName:                "books",
		ConcreteFieldInstructions: ConcreteFieldInstructions,
	}
}
