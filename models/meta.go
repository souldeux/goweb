package models

type ModelMetaManager interface {
	//The ModelMetaManager is an abstraction for things that represent
	//data in the database. It exposes methods useful for determining how
	//a given thing should interact with the DB in a variety of ways.

	//Things that implement the ModelMetaManager interface store and
	//express metadata about the database concept they represent in
	//their Meta() method
	Meta() *metadata
}

type metadata struct {
	//The name of the table to which this model's data belongs
	Table string `json:"table"`
	/*
			A map of instructions for creating columns in the database for this model.
			Example:
			ConcreteFieldInstructions := map[string]string{
			  "ID": "serial PRIMARY KEY",
				"Title": "VARCHAR (255) NOT NULL",
		  }
	*/
	ConcreteFieldInstructions map[string]string `json:"concreteFields"`

	//Help determine how to present singular/plural versions of this model when
	//pretty-printing stuff
	SingularName string `json:"singularName"`
	PluralName   string `json:"pluralName"`
}
