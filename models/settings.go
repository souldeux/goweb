package models

func AllModels() []ModelMetaManager {
	//AllModels returns a slice of ModelMetaManagers, each of which has a
	//Meta method that exposes instructions for setting the thing up in a DB.
	//Keeping this up-to-date is necessary for properly initializing all of the
	//tables in the DB
	return []ModelMetaManager{&Book{}, &User{}}
}
