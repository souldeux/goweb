package models

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (u *User) Meta() *metadata {
	ConcreteFieldInstructions := map[string]string{
		"ID":   "serial PRIMARY KEY",
		"Name": "VARCHAR (255) NOT NULL",
	}

	return &metadata{
		Table:                     "users",
		SingularName:              "user",
		PluralName:                "users",
		ConcreteFieldInstructions: ConcreteFieldInstructions,
	}

}
