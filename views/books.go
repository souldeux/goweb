package views

//for now, an exercise in what separation of concerns isn't

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"

	"simpleweb/models"
	"simpleweb/dbm"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	//IndexHandler returns a JSON representation of the books in the database
	//in response to a GET request
	//Grab all books from DB
	rows, err := dbm.DB.Query("SELECT * FROM Books")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	//Create an empty slice of Book structs. for each row, create a book and
	//add it to the slice
	var books []*models.Book
	for rows.Next() {
		var book models.Book
		err := rows.Scan(&book.ID, &book.Title, &book.Author)
		if err != nil {
			log.Fatal(err)
		}
		books = append(books, &book)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(books)
	if err != nil {
		log.Fatal(err)
	}
}

func CreateHandler(w http.ResponseWriter, r *http.Request) {
	//CreateHandler adds a new Book to the database
	w.WriteHeader(http.StatusCreated)
	//Initialize an empty Book
	var book models.Book
	//Decode the request body and dump it into the book
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&book)
	if err != nil {
		log.Fatal(err)
	}

	//jam book into table
	err = dbm.DB.QueryRow("INSERT INTO Books(Title, Author) VALUES($1, $2) RETURNING id, title, author", book.Title, book.Author).Scan(&book.ID, &book.Title, &book.Author)
	if err != nil {
		log.Fatal(err)
	}

	//write new book to the response writer
	err = json.NewEncoder(w).Encode(book)
	if err != nil {
		log.Fatal(err)
	}

}

func RetrieveHandler(w http.ResponseWriter, r *http.Request) {
	//RetrieveHandler fetches a single Book from the database
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "This is the Retrieve Handler. Vars: %v", vars)
}

func UpdateHandler(w http.ResponseWriter, r *http.Request) {
	//UpdateHandler updates a single Book's data
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "This is the Update Handler. Vars: %v", vars)
}

func DestroyHandler(w http.ResponseWriter, r *http.Request) {
	//DestroyHandler destroys a single Book
	vars := mux.Vars(r)
	//w.WriteHeader(http.StatusNoContent)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "This is the Destroy Handler. Vars: %v", vars)
}
