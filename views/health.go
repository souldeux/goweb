package views

import (
	"net/http"
)

func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	//HealthCheckHandler returns a bare 204 response
	w.WriteHeader(http.StatusNoContent)
}
