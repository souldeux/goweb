package dbm

import (
	"bytes"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"

	"simpleweb/models"
)

var DB *sql.DB

func InitializeDatabase() {
	//InitializeDatabase connects to a defined database,
	//pings the database to establish a robust connection,
	//re-initializes all tables (based on what Models are defined),
	//and seeds those tables with data when appropriate

	//Initialize the database and set global DB var
	//Connect to the local db
	connStr := "user=postgres password=password dbname=simpleweb host=postgres sslmode=disable"
	var err error
	log.Println("Opening postgres connection")
	DB, err = sql.Open("postgres", connStr) //don't shadow global
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Initializing Database: %v", DB_NAME)

	//Make sure the DB is doing the thing
	err = DB.Ping()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Database connection confirmed robust")

	log.Println("Acquiring registered models")
	registeredModels := models.AllModels()

	//Initialize a buffer that we will build, use, and wipe to make our initial
	//psql statements. These aren't properly parameterized since we're using
	//purely internal values - we're building unsafe strings, baby.

	for _, m := range registeredModels {
		go initializeModel(m, DB)
	}
}

func initializeModel(m models.ModelMetaManager, DB *sql.DB) {
	//initializeModel creates a clean table in the database for a given model
	//it does this by dropping the table defined by the model's metadata.Table, then
	//recreating it with the fields defined in metadata.ConcreteFieldInstructions
	var buffer bytes.Buffer
	metadata := m.Meta()
	log.Printf("Initializing table: %s", metadata.Table)

	//Drop this model's table if it currently exists.
	buffer.WriteString(fmt.Sprintf("DROP TABLE IF EXISTS %s", metadata.Table))
	log.Printf("Executing statement: %s", buffer.String())
	_, err := DB.Exec(buffer.String())
	if err != nil {
		log.Fatal(err)
	}
	buffer.Reset()

	//(re)create this model's table in our own image
	buffer.WriteString(fmt.Sprintf("CREATE TABLE %s(", metadata.Table))

	//Initialize a counter so we'll be able to tell when to
	//stop adding commas to our field definitions
	i := 0
	for k, v := range metadata.ConcreteFieldInstructions {
		buffer.WriteString(fmt.Sprintf(" %s %s", k, v))
		i++
		if i < len(metadata.ConcreteFieldInstructions) {
			buffer.WriteString(",")
		}
	}
	buffer.WriteString(" )")
	log.Printf("Executing statement: %s", buffer.String())
	_, err = DB.Exec(buffer.String())
	if err != nil {
		log.Fatal(err)
	}
	buffer.Reset()
	log.Printf("Table initialized: %s", metadata.Table)
}
