FROM golang:alpine as builder
RUN apk --no-cache --update add git
RUN mkdir -p /build/src/simpleweb
ADD . /build/src/simpleweb
WORKDIR /build/src/simpleweb

ENV GOPATH=/build

RUN go get -v -t \
  github.com/gorilla/mux \
  github.com/lib/pq

RUN go build -o /build/src/server ./server

FROM alpine
RUN apk --no-cache --update add curl
COPY --from=builder /build/src/server /opt/server/
WORKDIR /opt/server

HEALTHCHECK CMD curl --fail http://localhost:8080/health || exit 1
CMD ["./server"]
