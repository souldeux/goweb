/*
A simple web service
*/
package main

import (
	"context"
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"simpleweb/middleware"
	"simpleweb/dbm"
)

func main() {
	//First, initialize the database
	go dbm.InitializeDatabase()
	//I feel like there must be a better way to do this
	defer dbm.DB.Close()

	//Set a timeout duration
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*30, "the duration for which the server will gracefully wait for existing connections to finish")
	flag.Parse()

	//Define a new router and add routes
	r := mux.NewRouter()

	//Set routes using our helper function from routing.go
	SetRoutes(r)

	//Activate logging middleware
	r.Use(middleware.ConsoleLoggingMiddleware)

	//Set up a new server
	srv := &http.Server{
		Addr:         "0.0.0.0:8080",
		WriteTimeout: time.Second * 30,
		ReadTimeout:  time.Second * 30,
		IdleTimeout:  time.Second * 90,
		Handler:      r, //instance of gorilla/mux router
	}

	//Run the server in a goroutine so it doesn't block
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	//Accept graceful shutdowns when quit via SIGINT
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	//Block until SIGINT is received
	<-c

	//Create a deadline to wait for
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	//Doesn't block if no connections, otherwise waits until timeout
	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)

}
