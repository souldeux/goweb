package main

import (
	"github.com/gorilla/mux"
	"simpleweb/views"
)

func SetRoutes(r *mux.Router) {
	r.HandleFunc("/health", views.HealthCheckHandler).Methods("GET", "HEAD")
	r.HandleFunc("/books", views.IndexHandler).Methods("GET")
	r.HandleFunc("/books", views.CreateHandler).Methods("POST")
	r.HandleFunc("/books/{title}", views.RetrieveHandler).Methods("GET")
	r.HandleFunc("/books/{title}", views.UpdateHandler).Methods("PUT")
	r.HandleFunc("/books/{title}", views.DestroyHandler).Methods("DELETE")
}
